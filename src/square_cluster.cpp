#include <rubiks_cv/square_cluster.h>

int SquareCluster::green_ref[] = {129,78,167};
int SquareCluster::red_ref[] = {108,186,156};
int SquareCluster::blue_ref[] = {103,86,52};
int SquareCluster::yellow_ref[] = {248,100,226};
int SquareCluster::orange_ref[] = {151,194,181};
int SquareCluster::white_ref[] = {232,99,126};
int* SquareCluster::ref_colors[]= {&green_ref[0], &red_ref[0],
                                         &blue_ref[0],&yellow_ref[0], &orange_ref[0], &white_ref[0]};
// Luminance upper bound for cropped area
const int SquareCluster::CROP_LIGHT_THRESHOLD = 1;

// Luminance upper bound for black edges of cube
const int SquareCluster::DARK_EXPANSION_THRESHOLD = 60;

/** Get string representing colors in passed grid **/
std::string SquareCluster::squareCluster(Mat& img) {

    NNGrid grid(img);
//		std::cout << "Created NNGrid" << std::endl;
    // Holds indices corresponding to found reference colors
    int colors[9];

    // Find left edge
    int x_left = 0;
    int y_left = grid.height()/2;
    findCropEdge(grid, x_left, y_left, 1, 0);

    // Find top edge
    int x_top = grid.width()/2;
    int y_top = grid.height()-1;
    findCropEdge(grid, x_top, y_top, 0, -1);

    // Find right edge
    int x_right = grid.width()-1;
    int y_right = grid.height()/2;
    findCropEdge(grid, x_right, y_right, -1, 0);

    // Find bottom edge
    int x_bottom = grid.width()/2;
    int y_bottom = 0;
    findCropEdge(grid, x_bottom, y_bottom, 0, 1);

    // Estimate center of cube
    int x_center = (x_left+x_right)/2;
    int y_center = (y_top+y_bottom)/2;

//    std::cout << "About to expand center" << std::endl;
    // Find true center of center square and estimate its color
    Cluster center_square = expandUntilDark(grid, x_center, y_center);
//    std::cout << "Done expanding center: size = " << center_square.size << std::endl;

    // Get color of upper left square
    int x_upper_left_square = center_square.x - center_square.size;
    int y_upper_left_square = center_square.y - center_square.size;
    colors[0] = getDominantColor(grid, x_upper_left_square, y_upper_left_square, center_square.size/2);
//    std::cout << "Found upper left at (" <<x_upper_left_square<<","<<y_upper_left_square<<")" << std::endl;

    // Get color of upper center square
    int x_upper_center_square = center_square.x;
    int y_upper_center_square = center_square.y - center_square.size;
    colors[1] = getDominantColor(grid, x_upper_center_square, y_upper_center_square, center_square.size/2);
//    std::cout << "Found upper center at (" <<x_upper_center_square<<","<<y_upper_center_square<<")" << std::endl;

    // Get color of upper right square
    int x_upper_right_square = center_square.x + center_square.size;
    int y_upper_right_square = center_square.y - center_square.size;
    colors[2] = getDominantColor(grid, x_upper_right_square, y_upper_right_square, center_square.size/2);
//    std::cout << "Found upper right at (" <<x_upper_right_square<<","<<y_upper_right_square<<")" << std::endl;

    // Get color of middle left square
    int x_middle_left_square = center_square.x - center_square.size;
    int y_middle_left_square = center_square.y;
    colors[3] = getDominantColor(grid, x_middle_left_square, y_middle_left_square, center_square.size/2);
//    std::cout << "Found middle left at (" <<x_middle_left_square<<","<<y_middle_left_square<<")" << std::endl;

    // Already have color of center square
    colors[4] = getClosestRef(center_square.avgColor);
//    std::cout << "Found center at (" <<x_center<<","<<y_center<<")" << std::endl;

    // Get color of
    int x_middle_right_square = center_square.x + center_square.size;
    int y_middle_right_square = center_square.y;
    colors[5] = getDominantColor(grid, x_middle_right_square, y_middle_right_square, center_square.size/2);
//    std::cout << "Found middle right at (" <<x_middle_right_square<<","<<y_middle_right_square<<")" << std::endl;

    // Get color of lower left square
    int x_lower_left_square = center_square.x - center_square.size;
    int y_lower_left_square = center_square.y + center_square.size;
    colors[6] = getDominantColor(grid, x_lower_left_square, y_lower_left_square, center_square.size/2);
//    std::cout << "Found lower left at (" <<x_lower_left_square<<","<<y_lower_left_square<<")" << std::endl;

    // Get color of lower center square
    int x_lower_center_square = center_square.x;
    int y_lower_center_square = center_square.y + center_square.size;
    colors[7] = getDominantColor(grid, x_lower_center_square, y_lower_center_square, center_square.size/2);
//    std::cout << "Found lower center at (" <<x_lower_center_square<<","<<y_lower_center_square<<")" << std::endl;

    // Get color of lower right square
    int x_lower_right_square = center_square.x + center_square.size;
    int y_lower_right_square = center_square.y + center_square.size;
    colors[8] = getDominantColor(grid, x_lower_right_square, y_lower_right_square, center_square.size/2);
//    std::cout << "Found lower right at (" <<x_lower_right_square<<","<<y_lower_right_square<<")" << std::endl;

//    printResult(&colors[0]);
    return parseResult(&colors[0]);
}

/** Build the result string from the found color indices **/
std::string SquareCluster::parseResult(int* colors) {
    std::string result("");

    for(int i = 0; i < 9; i++) {
        switch(colors[i]) {
        case 0:
            result.push_back('G');
            break;
        case 1:
            result.push_back('R');
            break;
        case 2:
            result.push_back('B');
            break;
        case 3:
            result.push_back('Y');
            break;
        case 4:
            result.push_back('O');
            break;
        case 5:
            result.push_back('W');
            break;
        default:
            result.push_back('?');
        }
    }

    return result;
}

/** Print result to the console **/
void SquareCluster::printResult(int* colors) {
    std::string result(parseResult(colors));

    // Print out result string in 3x3 square
    for(int i = 0; i < result.size(); i+=3) {
        std::cout << result.substr(i, std::min(3, ((int)result.size()) - i)) << std::endl;
    }
}

/** Find the crop edge by inching towards the crop-image intersection
  grid: The grid representing the image
  x: The x coordinate to start the search at
     Will hold x coordinate of crop edge upon return
  y: The y coordinate to start the search at
     Will hold y coordinate of crop edge upon return
  dx: How much to increment in x direction for each iteration
  dy: How much to increment in y direction for each iteration **/
bool SquareCluster::findCropEdge(NNGrid& grid, int& x,
                                 int& y, int dx, int dy) {

    int* vals = NULL;
    while(x >= 0 && x < grid.width() && y >= 0 && y < grid.height()) {
        vals = grid.getNode(x,y)->vals;

        // Check if we have hit a non-black pixel
        if(vals[0] >= CROP_LIGHT_THRESHOLD) {
            return true;
        }
        x += dx;
        y += dy;
    }

    return false;
}

/** Find the edge of a square by inching towards a dark edge
  grid: The grid representing the image
  x: The x coordinate to start the search at
     Will hold x coordinate of edge upon return
  y: The y coordinate to start the search at
     Will hold y coordinate of edge upon return
  dx: How much to increment in x direction for each iteration
  dy: How much to increment in y direction for each iteration
  itr_limit: Limits the number of iterations done in any direction **/
int* SquareCluster::findColorEdge(NNGrid& grid, int& x,
                                     int& y, int dx, int dy, int itr_limit) {
    int* cur_color = NULL;
    int* dom_color = NULL;
    int dom_color_mag = -1;
    int itr = 0;

    while(x >= 0 && x < grid.width() && y >= 0 && y < grid.height()) {
        cur_color = grid.getNode(x,y)->vals;

        // Stop if a dark pixel has been reached
        if(cur_color[0] <= DARK_EXPANSION_THRESHOLD) {
            return dom_color != NULL ? dom_color : cur_color;
        }

        // Stop if iteration limit reached
        if(itr_limit >= 0 && itr >= itr_limit) {
            return dom_color != NULL ? dom_color : cur_color;
        }

        // Compute magnitude of u,v components
        int cur_color_mag = std::pow(((358.0/255.0)*cur_color[1])-134,2) + std::pow(((262.0/255.0)*cur_color[2])-140,2);

        // Find color with largest u,v magnitude
        if(cur_color_mag > dom_color_mag) {
            dom_color = cur_color;
            dom_color_mag = cur_color_mag;
        }

        // Increment
        x += dx;
        y += dy;
        itr++;
    }

    return dom_color;

}

/** Returns the index that the passed color is closest too **/
int SquareCluster::getClosestRef(int* color) {
    int min_dist = INT_MAX;
    int result = -1;

    for(int i = 0; i < 6; i++) {
        int dist = 0;
        for(int j = 1; j < 3; j++) {
            dist += std::pow(color[j]-ref_colors[i][j],2);
        }
 /*       int dist = std::pow((358.0/255.0)*(color[1] - ref_colors[i][1]), 2) +
                   std::pow((262.0/255.0)*(color[2] - ref_colors[i][2]), 2);*/
        if(dist < min_dist) {
            min_dist = dist;
            result = i;
        }
    }

    if(result == 0 && color[2] < 145) {
        return 5;
    }

    return result;
}

/** Iterate horizontally and vertically to find the dominant color in the square
    grid: Represents the image
    x_query: Approximate x coordinate of the center of the square
    y_query: Approximate y coordinate of the center of the square
    itr_limit: Limits the number of iterations allowed **/
int SquareCluster::getDominantColor(NNGrid& grid, int x_query, int y_query, int itr_limit) {

    // Search left
    int x_left = x_query;
    int y_left = y_query;
    int* left_dom_color = findColorEdge(grid, x_left, y_left, -1, 0, itr_limit);

    // Search right
    int x_right = x_query;
    int y_right = y_query;
    int* right_dom_color = findColorEdge(grid, x_right, y_right, 1, 0, itr_limit);

    // Search up
    int x_top = x_query;
    int y_top = y_query;
    int* top_dom_color = findColorEdge(grid, x_top, y_top, 0, 1, itr_limit);

    // Search down
    int x_bottom = x_query;
    int y_bottom = y_query;
    int* bottom_dom_color = findColorEdge(grid, x_bottom, y_bottom, 0, -1, itr_limit);

//    int cur_color_mag = std::pow(((358.0/255.0)*cur_color[1])-134,2) + std::pow(((262.0/255.0)*cur_color[2])-140,2) > dom_color_mag;

    int color[3];
    for(int i = 0; i < 3; i++) {
        color[i] = (left_dom_color[i] + right_dom_color[i] + top_dom_color[i] + bottom_dom_color[i])/4;
    }

    return getClosestRef(&color[0]);

}

/** Expand horizontally and vertically until a dark pixel is encountered
    grid: Represents the image to be searched
    x_query: Approximate x coordinate of center
    y_query: Approximate y coordinate of center **/
Cluster SquareCluster::expandUntilDark(NNGrid& grid, int x_query, int y_query) {

    // Expand left
    int x_left = x_query;
    int y_left = y_query;
    int* left_dom_color = findColorEdge(grid, x_left, y_left, -1, 0);

    // Expand right
    int x_right = x_query;
    int y_right = y_query;
    int* right_dom_color = findColorEdge(grid, x_right, y_right, 1, 0);

    // Expand up
    int x_top = x_query;
    int y_top = y_query;
    int* top_dom_color = findColorEdge(grid, x_top, y_top, 0, 1);

    // Expand down
    int x_bottom = x_query;
    int y_bottom = y_query;
    int* bottom_dom_color = findColorEdge(grid, x_bottom, y_bottom, 0, -1);

    // Estimate width and height
    int width = x_right-x_left;
    int height = y_top - y_bottom;

    if(width > height) {
        // Re-estimate height in case y_query was off and there is rotation
        x_top = (x_right+x_left)/2;
        y_top = y_query;
        top_dom_color = findColorEdge(grid, x_top, y_top, 0 , 1);

        x_bottom = (x_right+x_left)/2;
        y_bottom = y_query;
        bottom_dom_color = findColorEdge(grid, x_bottom, y_bottom, 0, -1);
    } else {
        //Re-estimate width in case x_query was off and there is rotation
        x_left = x_query;
        y_left = (y_top+y_bottom) / 2;
        left_dom_color = findColorEdge(grid, x_left, y_left, -1, 0);

        x_right = x_query;
        y_right = (y_top+y_bottom) / 2;
        right_dom_color = findColorEdge(grid, x_right, y_right, 1,0);
    }

    int size = std::max(x_right-x_left, y_top - y_bottom);
    int color[3];
    // Average dominant colors found in searches in each direction
    for(int i = 0; i < 3; i++) {
        color[i] = (left_dom_color[i] + right_dom_color[i] + top_dom_color[i] + bottom_dom_color[i])/4;
    }

    return Cluster(&color[0], size, (x_right+x_left)/2, (y_top+y_bottom)/2);

}

const static std::string img1_name = "/home/planc509/cube_ws/src/rubiks_cv/data/cropped2/face1.png";
int main(int argc, char** argv) {

    Mat img1;
    if(argc >= 2) {
        img1 = imread(argv[1], CV_LOAD_IMAGE_COLOR);
    } else {
        img1 = imread(img1_name, CV_LOAD_IMAGE_COLOR);
    }
    Mat luv_img = img1.clone();

    cvtColor(img1, luv_img, CV_BGR2Luv);
//    NNGrid grid(luv_img);
    SquareCluster::squareCluster(luv_img);
//    Mat img_out;
//    grid.gridToImg(img_out);

    imshow("original", img1);
 //   imshow("result", img_out);
    waitKey(0);

}
