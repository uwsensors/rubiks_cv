#include <rubiks_cv/kMeans.h>

void KMeans::kMeans(vector<Cluster*>& clusters, vector<vector<Cluster*>*>& cube,
                    int k, std::vector<std::vector<int> > ref_colors) {
    vector<Cluster*> means;
    initMeans(means, ref_colors);

    int changes = 1;
    while(changes > 0) {
        changes = assignClusters(clusters, means);
        updateMeans(clusters, means);
    }
    vector<vector<Cluster*>*> candidates;
    getCandidates(clusters, candidates);

    organizeCandidatesByPhoto(candidates, cube);

    for(int i = 0; i < means.size(); i++) {
        delete [] means[i]->avgColor;
        delete means[i];
    }
}

void KMeans::initMeans(std::vector<Cluster*>& means, std::vector<std::vector<int> > ref_colors) {
    if(ref_colors.empty()) {
        int* red = new int[3];
        red[0] = 0;
        red[1] = 0;
        red[2] = 204;

        int* orange = new int[3];
        orange[0] = 0;
        orange[1] = 51;
        orange[2] = 255;

        int* green = new int[3];
        green[0] = 0;
        green[1] = 102;
        green[2] = 0;

        int* yellow = new int[3];
        yellow[0] = 0;
        yellow[1] = 204;
        yellow[2] = 255;

        int* blue = new int[3];
        blue[0] = 153;
        blue[1] = 51;
        blue[2] = 0;

        int* white = new int[3];
        white[0] = 255;
        white[1] = 255;
        white[2] = 255;

        means.push_back(new Cluster(red,0));
        means.push_back(new Cluster(orange,1));
        means.push_back(new Cluster(green,2));
        means.push_back(new Cluster(yellow,3));
        means.push_back(new Cluster(blue,4));
        means.push_back(new Cluster(white,5));
    } else {
        int* color = (int*) malloc(3*sizeof(int));
        for(int i = 0; i < 6; i++) {
            for(int j = 0; j < 3; j++) {
                color[j] = ref_colors[i][j];
            }
            means.push_back(new Cluster(color,i));
        }
        free(color);
    }
}

int KMeans::assignClusters(vector<Cluster*>& clusters, vector<Cluster*>& means) {
    int changes = 0;
    Cluster* tmpCluster;
    for(int i = 0; i < means.size(); i++) {
        means[i]->size = 0;
    }

    for(int i = 0; i < clusters.size(); i++) {
        tmpCluster = clusters[i];
        double minDist = 3*255*255+1;
        int meanIdx = -1;
        for(int j = 0; j < means.size(); j++) {
            double dist = distance(tmpCluster, means[j]);
            if(dist < minDist) {
                minDist = dist;
                meanIdx = j;
            }
        }
        if(tmpCluster->assignedColor != meanIdx) {
            changes++;
        }
        tmpCluster->assignedColor = meanIdx;
        tmpCluster->meanDist = minDist;
        means[meanIdx]->size++;
    }
    return changes;
}

void KMeans::updateMeans(vector<Cluster*>& clusters, vector<Cluster*>& means) {
    Cluster* tmpCluster;
    vector<double*> meanColors;
    for(int i = 0; i < means.size(); i++) {
        double* rgb = new double[3];
        rgb[0] = 0.0;
        rgb[1] = 0.0;
        rgb[2] = 0.0;
        meanColors.push_back(rgb);
    }

    for(int i = 0; i < clusters.size(); i++) {
        tmpCluster = clusters[i];
        double* rgb = meanColors[tmpCluster->assignedColor];
        rgb[0] += tmpCluster->avgColor[0];
        rgb[1] += tmpCluster->avgColor[1];
        rgb[2] += tmpCluster->avgColor[2];
    }

    for(int i = 0; i < means.size(); i++) {

        means[i]->avgColor[0] = (int) (meanColors[i][0]/means[i]->size);
        means[i]->avgColor[1] = (int) (meanColors[i][1]/means[i]->size);
        means[i]->avgColor[2] = (int) (meanColors[i][2]/means[i]->size);
    }

    for(int i = 0; i < meanColors.size(); i++) {
        delete [] meanColors[i];
    }

}

double KMeans::distance(Cluster* x, Cluster* mean) {

    double blueDiff = x->avgColor[0] - mean->avgColor[0];
    double greenDiff = x->avgColor[1] - mean->avgColor[1];
    double redDiff = x->avgColor[2] - mean->avgColor[2];

    return blueDiff*blueDiff + greenDiff*greenDiff + redDiff*redDiff;
}

void KMeans::getCandidates(vector<Cluster*>& clusters, vector<vector<Cluster*>*>& candidates) {
    Cluster* tmpCluster;
    int k = 6;
    for(int i = 0; i < k; i++) {
        candidates.push_back(new vector<Cluster*>());
    }
    for(int i = 0; i < clusters.size(); i++) {
        tmpCluster = clusters[i];
        candidates[tmpCluster->assignedColor]->push_back(tmpCluster);
    }
    if(KMEANS_DEBUG) {
        for(int i = 0; i < candidates.size(); i++) {
            ROS_INFO("Found %d candidates for color %d", candidates[i]->size(), i);
        }
    }

    vector<Cluster*>* curList;
    for(int i = 0; i < k; i++) {
        curList = candidates[i];
        for(int j = 0; j < curList->size(); j++) {
            for(int l = j+1; l < curList->size(); l++) {
                if((*curList)[j]->meanDist > (*curList)[l]->meanDist) {
                    tmpCluster = (*curList)[l];
                    (*curList)[l] = (*curList)[j];
                    (*curList)[j] = tmpCluster;
                }
            }
        }
        while(curList->size() > 9) {
            curList->erase(curList->end()-1);
        }
        while(curList->size() < 9) {
            tmpCluster = new Cluster(NULL, -1);
            curList->push_back(tmpCluster);
            clusters.push_back(tmpCluster);
        }
    }

}

void KMeans::organizeCandidatesByPhoto(vector<vector<Cluster*>*>& candidates, vector<vector<Cluster*>*>& cube) {
    Cluster* tmpCluster;
    for(int i = 0; i < 6; i++) {
        cube.push_back(new vector<Cluster*>());
    }
    vector<Cluster*>* curList;
    for(int i = 0; i < candidates.size(); i++) {
        curList = candidates[i];
        for(int j = 0; j < curList->size(); j++) {
            tmpCluster = (*curList)[j];
            if(tmpCluster->assignedColor >= 0) {
                cube[tmpCluster->photoID]->push_back(tmpCluster);
            }
        }
    }
    for(int i = 0; i < cube.size(); i++) {
        curList = cube[i];
        while(curList->size() > 9) {
            curList->erase(curList->end()-1);
        }
//        while(curList->size() < 9) {
//            curList->push_back(new Cluster(NULL,-1));
//        }
    }
    for(int i = 0; i < candidates.size(); i++) {
        delete candidates[i];
    }
}
