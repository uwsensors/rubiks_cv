#include <rubiks_cv/node.h>

using namespace std;

Node::Node(unsigned int x, unsigned int y, int* vals): x(x), y(y), vals(vals), cluster(-1) {}
Node::~Node() {}
void Node::assignToCluster(int cluster) {
    this->cluster = cluster;
}
bool Node::isSimilarColor(const Node& other, double rThresh, double gThresh, double bThresh) {
    return abs(vals[0] - other.vals[0]) < rThresh && abs(vals[1]-other.vals[1]) < gThresh && abs(vals[2]-other.vals[2]) < bThresh;
}
