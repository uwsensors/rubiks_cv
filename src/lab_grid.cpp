#include <rubiks_cv/lab_grid.h>

LABGrid::LABGrid(Mat& img): w((unsigned int)img.cols), h((unsigned int)img.rows), channels((unsigned int)img.channels()), type(img.type()) {
    grid = new LABNode*[w*h];

    for(int i = 0; i < h; i++) {
        for(int j = 0; j < w; j++) {
            Vec3b channel_vals = img.at<Vec3b>(i,j);
            int* lab = new int[3];
            for(int k = 0; k < 3; k++) {
                lab[k] = channel_vals[k];
            }
            grid[i*w+j] = new LABNode(j,i, lab);

        }
    }


}

LABGrid::~LABGrid() {
    LABNode* node;
    for(int i = 0; i < w; i++) {
        for(int j = 0; j < h; j++) {
            node = grid[j*w+i];
            delete [] node->lab;
            delete node;
        }
    }
    delete grid;

}

void LABGrid::getNeighbors(unsigned int x, unsigned int y, vector<LABNode*>& neighbors) {

    int xd = 1;
    int yd = 1;


    for(int i = -xd; i <= xd; i++) {
        for(int j = -yd; j <= yd; j++) {
            if(i == 0 && j == 0) {
                continue;
            }
            if(x+i >= 0 && x+i < w && y+j >= 0 && y+j < h && grid[(y+j)*w+(x+i)]->cluster < 0) {
                neighbors.push_back(grid[(y+j)*w+(x+i)]);

            }
        }
    }

}

LABNode* LABGrid::getNode(unsigned int x, unsigned int y) {
    return grid[y*w+x];
}

void LABGrid::gridToImg(Mat &img_out) {
    img_out = cvCreateMat(h,w,type);

    for(int i = 0; i < h; i++) {
        for(int j = 0; j < w; j++) {
            int* node_vals = getNode(j,i)->lab;
            Vec3b channels(node_vals[0], node_vals[1], node_vals[2]);
            img_out.at<Vec3b>(i,j) = channels;
        }
    }


}
