#include <ros/ros.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <rubiks_cv/euclideanCluster.h>
#include <rubiks_cv/kMeans.h>
#include <rubiks_cv/cluster.h>
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <rubiks_cv/CubeState.h>
#include <rubiks_cv/colorConversion.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;
using namespace std;

const static string TEST_IMG_1 = "/home/planc509/cube_ws/src/rubiks_cv/data/cropped1/face1.png";
const static string TEST_IMG_2 = "/home/planc509/cube_ws/src/rubiks_cv/data/cropped1/face2.png";
const static string TEST_IMG_3 = "/home/planc509/cube_ws/src/rubiks_cv/data/cropped1/face3.png";
const static string TEST_IMG_4 = "/home/planc509/cube_ws/src/rubiks_cv/data/cropped1/face4.png";
const static string TEST_IMG_5 = "/home/planc509/cube_ws/src/rubiks_cv/data/cropped1/face5.png";
const static string TEST_IMG_6 = "/home/planc509/cube_ws/src/rubiks_cv/data/cropped1/face6.png";

const static int MIN_CLUSTER_SIZE = 100;
const static int MAX_CLUSTER_SIZE = 3000;

const static double SIGMA = 1.0;

void addClusters(vector<Cluster*>& clusters, vector<Cluster*>& candidates, int photoID);
void markCenters(Mat& img,vector<Cluster*>* centers);
void sortByRowThenColumn(vector<Cluster*>* face);
std::string getFace(vector<Cluster*>* face);

bool offer_service;
bool save;
std::string save_prefix;

std::string computeCubeState(Mat& img1, Mat& img2, Mat& img3, Mat& img4, Mat& img5, Mat& img6) {

    Mat blur1;
    GaussianBlur( img1, blur1, Size( 0, 0 ), SIGMA, SIGMA );

    Mat blur2;
    GaussianBlur( img2, blur2, Size( 0, 0 ), SIGMA, SIGMA );

    Mat blur3;
    GaussianBlur( img3, blur3, Size( 0, 0 ), SIGMA, SIGMA );

    Mat blur4;
    GaussianBlur( img4, blur4, Size( 0, 0 ), SIGMA, SIGMA );

    Mat blur5;
    GaussianBlur( img5, blur5, Size( 0, 0 ), SIGMA, SIGMA );

    Mat blur6;
    GaussianBlur( img6, blur6, Size( 0, 0 ), SIGMA, SIGMA );

    vector<Cluster*> clusters;
    vector<Cluster*> clusters1;
    vector<Cluster*> clusters2;
    vector<Cluster*> clusters3;
    vector<Cluster*> clusters4;
    vector<Cluster*> clusters5;
    vector<Cluster*> clusters6;
    std::string result = "";

    Cluster* cluster;
    namedWindow("Display window", WINDOW_NORMAL);

    euclideanCluster(blur1, clusters1);
    addClusters(clusters, clusters1, 0);
    imshow("Display window", blur1);
    waitKey(0);

    euclideanCluster(blur2, clusters2);
    addClusters(clusters, clusters2, 1);
    imshow("Display window", blur2);
    waitKey(0);


    euclideanCluster(blur3, clusters3);
    addClusters(clusters, clusters3, 2);
    imshow("Display window", blur3);
    waitKey(0);


    euclideanCluster(blur4, clusters4);
    addClusters(clusters, clusters4, 3);
    imshow("Display window", blur4);
    waitKey(0);


    euclideanCluster(blur5, clusters5);
    addClusters(clusters, clusters5, 4);
    imshow("Display window", blur5);
    waitKey(0);


    euclideanCluster(blur6, clusters6);
    addClusters(clusters, clusters6, 5);
    imshow("Display window", blur6);
    waitKey(0);


    vector<vector<Cluster*>*> cube;
    std::vector<std::vector<int> > ref_colors;
    ColorConversion::getLabRefColors(ref_colors);
    KMeans::kMeans(clusters, cube, 6, ref_colors);

    for(int i = 0; i<cube.size(); i++) {
        sortByRowThenColumn(cube[i]);
        result+=getFace(cube[i]);
    }

    if(!offer_service) {
        markCenters(blur1, cube[0]);
        markCenters(blur2, cube[1]);
        markCenters(blur3, cube[2]);
        markCenters(blur4, cube[3]);
        markCenters(blur5, cube[4]);
        markCenters(blur6, cube[5]);
    }

    for(int i = 0; i < clusters1.size(); i++) {
        delete [] clusters1[i]->avgColor;
        delete clusters1[i];
    }
    for(int i = 0; i < clusters2.size(); i++) {
        delete [] clusters2[i]->avgColor;
        delete clusters2[i];
    }
    for(int i = 0; i < clusters3.size(); i++) {
        delete [] clusters3[i]->avgColor;
        delete clusters3[i];
    }
    for(int i = 0; i < clusters4.size(); i++) {
        delete [] clusters4[i]->avgColor;
        delete clusters4[i];
    }
    for(int i = 0; i < clusters5.size(); i++) {
        delete [] clusters5[i]->avgColor;
        delete clusters5[i];
    }
    for(int i = 0; i < clusters6.size(); i++) {
        delete [] clusters6[i]->avgColor;
        delete clusters6[i];
    }
    for(int i = 0; i < clusters.size(); i++) {
        delete [] clusters[i]->avgColor;
        delete clusters[i];
    }
    for(int i = 0; i < cube.size(); i++ ) {
        delete cube[i];
    }

    if(!offer_service) {
        imshow("Display window", blur1);
        waitKey(0);
        imshow("Display window", blur2);
        waitKey(0);
        imshow("Display window", blur3);
        waitKey(0);
        imshow("Display window", blur4);
        waitKey(0);
        imshow("Display window", blur5);
        waitKey(0);
        imshow("Display window", blur6);
        waitKey(0);
    }

    return result;
}

bool cubeStateService(rubiks_cv::CubeState::Request &req, rubiks_cv::CubeState::Response& res) {

    cv_bridge::CvImagePtr cv1;
    cv_bridge::CvImagePtr cv2;
    cv_bridge::CvImagePtr cv3;
    cv_bridge::CvImagePtr cv4;
    cv_bridge::CvImagePtr cv5;
    cv_bridge::CvImagePtr cv6;
    const sensor_msgs::Image constImg1 = req.img1;
    const sensor_msgs::Image constImg2 = req.img2;
    const sensor_msgs::Image constImg3 = req.img3;
    const sensor_msgs::Image constImg4 = req.img4;
    const sensor_msgs::Image constImg5 = req.img5;
    const sensor_msgs::Image constImg6 = req.img6;
    try {

        cv1 = cv_bridge::toCvCopy(constImg1, sensor_msgs::image_encodings::BGR8);
        imwrite( save_prefix + "/img1.jpg", cv1->image );

        cv2 = cv_bridge::toCvCopy(constImg2, sensor_msgs::image_encodings::BGR8);
        imwrite( save_prefix + "/img2.jpg", cv2->image );

        cv3 = cv_bridge::toCvCopy(constImg3, sensor_msgs::image_encodings::BGR8);
        imwrite( save_prefix + "/img3.jpg", cv3->image );

        cv4 = cv_bridge::toCvCopy(constImg4, sensor_msgs::image_encodings::BGR8);
        imwrite( save_prefix + "/img4.jpg", cv4->image );

        cv5 = cv_bridge::toCvCopy(constImg5, sensor_msgs::image_encodings::BGR8);
        imwrite( save_prefix + "/img5.jpg", cv5->image );

        cv6 = cv_bridge::toCvCopy(constImg6, sensor_msgs::image_encodings::BGR8);
        imwrite( save_prefix + "/img6.jpg", cv6->image );

        if(save) {
            ROS_INFO("Going to save images in %s", save_prefix.c_str());
        }
    } catch (cv_bridge::Exception& e) {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return false;
    }
    res.state = computeCubeState(cv1->image,cv2->image,cv3->image,cv4->image,cv5->image, cv6->image);
    return true;
}

int main(int argc, char** argv) {

    ros::init(argc, argv, "rubiks_vision");
    ros::NodeHandle node;
    offer_service = true;
    save = false;
    save_prefix = "";
    node.param("/rubiks_vision/offer_service", offer_service, offer_service);
    node.param("/rubiks_vision/save_images", save, save);
    node.param("/rubiks_vision/save_prefix", save_prefix, save_prefix);
    ROS_INFO("offer_service: %d", offer_service);
    ROS_INFO("save_images: %d", save);
    ROS_INFO("save_prefix: %s", save_prefix.c_str());
    ros::ServiceServer cubeService;
    if(!offer_service) {
        ROS_INFO("Loading Images...");
        Mat img1 = imread(TEST_IMG_1, CV_LOAD_IMAGE_COLOR);
        Mat img2 = imread(TEST_IMG_2, CV_LOAD_IMAGE_COLOR);
        Mat img3 = imread(TEST_IMG_3, CV_LOAD_IMAGE_COLOR);
        Mat img4 = imread(TEST_IMG_4, CV_LOAD_IMAGE_COLOR);
        Mat img5 = imread(TEST_IMG_5, CV_LOAD_IMAGE_COLOR);
        Mat img6 = imread(TEST_IMG_6, CV_LOAD_IMAGE_COLOR);
        ROS_INFO("...done loading images");

        Mat lab_img1 = img1.clone();
        cvtColor(img1, lab_img1, CV_BGR2Lab);

        Mat lab_img2 = img2.clone();
        cvtColor(img2, lab_img2, CV_BGR2Lab);

        Mat lab_img3 = img3.clone();
        cvtColor(img3, lab_img3, CV_BGR2Lab);

        Mat lab_img4 = img4.clone();
        cvtColor(img4, lab_img4, CV_BGR2Lab);

        Mat lab_img5 = img5.clone();
        cvtColor(img5, lab_img5, CV_BGR2Lab);

        Mat lab_img6 = img6.clone();
        cvtColor(img6, lab_img6, CV_BGR2Lab);

        computeCubeState(lab_img1, lab_img2, lab_img3, lab_img4, lab_img5, lab_img6);

    } else {
        cubeService = node.advertiseService("get_cube_state", cubeStateService);
        ROS_INFO("get_cube_state service is active");
    }

    ros::spin();

}

void addClusters(vector<Cluster*>& clusters, vector<Cluster*>& candidates, int photoID) {

    int count = 0;
    Cluster* cluster;
    for(int i = 0; i < candidates.size(); i++) {
        cluster = candidates[i];
        if(cluster->size > MIN_CLUSTER_SIZE && cluster->size < MAX_CLUSTER_SIZE) {
            cluster->photoID = photoID;
            clusters.push_back(cluster);
            candidates.erase(candidates.begin()+i);
            i--;
            count++;
        }
    }
    ROS_INFO("Added %d clusters from photo %d", count, photoID);

}

void markCenters(Mat& img,vector<Cluster*>* centers) {

    uchar* pixel;
    for(int i = 0; i < centers->size(); i++) {
        pixel = img.ptr<uchar>((*centers)[i]->y);
        for(int j = 0; j < 3; j++) {
            *(pixel+3*((int)(*centers)[i]->x)+j) = 0;
            if((*centers)[i]->x-1 >= 0) {
                *(pixel+3*((int)(*centers)[i]->x-1)+j) = 0;
            }
            if((*centers)[i]->x+1 < img.cols) {
                *(pixel+3*((int)(*centers)[i]->x+1)+j) = 0;
            }

        }
    }
}


void sortByRowThenColumn(vector<Cluster*>* face) {
    Cluster* tmpCluster;
    for(int i = 0; i < face->size(); i++) {
        for(int j = i+1; j < face->size(); j++) {
            if((*face)[i]->y > (*face)[j]->y) {
                tmpCluster = (*face)[j];
                (*face)[j] = (*face)[i];
                (*face)[i] = tmpCluster;
            }
        }
    }
    for(int i = 0; i < 3; i++) {
        for(int j = 0; j < 3; j++) {
            for(int k = j+1; k<3; k++) {
                if((*face)[3*i+j]->x > (*face)[3*i+k]->x ) {
                    tmpCluster = (*face)[3*i+j];
                    (*face)[3*i+j] = (*face)[3*i+k];
                    (*face)[3*i+k] = tmpCluster;
                }

            }
        }
    }
}

std::string getFace(vector<Cluster*>* face) {
    string result = "";
    for(int i = 0; i < face->size(); i++) {
        switch((*face)[i]->assignedColor) {
        case 0:
            result += "R";
            break;
        case 1:
            result += "O";
            break;
        case 2:
            result += "G";
            break;
        case 3:
            result += "Y";
            break;
        case 4:
            result += "B";
            break;
        case 5:
            result += "W";
            break;
           default:
            result+= "ERROR";
            break;

        }
    }
    ROS_INFO("%s",result.c_str());
    return result;
}
