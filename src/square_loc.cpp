/**
 * Derived from https://github.com/bsdnoobz/opencv-code/blob/master/shape-detect.cpp
 * Simple shape detector program.
 * It loads an image and tries to find simple shapes (rectangle, triangle, circle, etc) in it.
 * This program is a modified version of `squares.cpp` found in the OpenCV sample dir.
 */
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <cmath>
#include <iostream>
#include <ros/ros.h>
#include <stdlib.h>

const static std::string TEST_IMG_1 = "/home/patrick/cube_ws/src/rubiks_cv/data/high_light/face1.bmp";
const static std::string TEST_IMG_2 = "/home/patrick/cube_ws/src/rubiks_cv/data/high_light/face2.bmp";
const static std::string TEST_IMG_3 = "/home/patrick/cube_ws/src/rubiks_cv/data/high_light/face3.bmp";
const static std::string TEST_IMG_4 = "/home/patrick/cube_ws/src/rubiks_cv/data/high_light/face4.bmp";
const static std::string TEST_IMG_5 = "/home/patrick/cube_ws/src/rubiks_cv/data/high_light/face5.bmp";
const static std::string TEST_IMG_6 = "/home/patrick/cube_ws/src/rubiks_cv/data/high_light/face6.bmp";

class SquareLoc {
public:
    SquareLoc(cv::Mat& img);
    ~SquareLoc();

    static double angle(cv::Point pt1, cv::Point pt2, cv::Point pt0);
    void setLabel(cv::Mat& im, const std::string label,
                  std::vector<cv::Point>& contour);
    void findSquares();
    void showSquares();
    cv::Mat img;
    cv::Mat canny;
    std::vector< std::vector<cv::Point> > squares;
    std::vector<std::vector<cv::Point> > contours;

};

SquareLoc::SquareLoc(cv::Mat& img) : img(img) {

}

SquareLoc::~SquareLoc() {

}

/**
 * Helper function to find a cosine of angle between vectors
 * from pt0->pt1 and pt0->pt2
 */
double SquareLoc::angle(cv::Point pt1, cv::Point pt2, cv::Point pt0) {
    double dx1 = pt1.x - pt0.x;
    double dy1 = pt1.y - pt0.y;
    double dx2 = pt2.x - pt0.x;
    double dy2 = pt2.y - pt0.y;
    return (dx1*dx2 + dy1*dy2)/sqrt((dx1*dx1 + dy1*dy1)*(dx2*dx2 + dy2*dy2) + 1e-10);
}

/**
 * Helper function to display text in the center of a contour
 */
void SquareLoc::setLabel(cv::Mat& im, const std::string label, std::vector<cv::Point>& contour) {
    int fontface = cv::FONT_HERSHEY_SIMPLEX;
    double scale = 0.4;
    int thickness = 1;
    int baseline = 0;

    cv::Size text = cv::getTextSize(label, fontface, scale, thickness, &baseline);
    cv::Rect r = cv::boundingRect(contour);

    cv::Point pt(r.x + ((r.width - text.width) / 2), r.y + ((r.height + text.height) / 2));
    cv::rectangle(im, pt + cv::Point(0, baseline), pt + cv::Point(text.width, -text.height), CV_RGB(255,255,255), CV_FILLED);
    cv::putText(im, label, pt, fontface, scale, CV_RGB(0,0,0), thickness, 8);
}

void SquareLoc::findSquares() {
    if(img.empty()) {
        std::cout << "img was empty" << std::endl;
        return;
    }

    cv::Mat gray, gray_blur;
    cv::cvtColor(img, gray, CV_BGR2GRAY);
    cv::Size size;
    size.height = 5;
    size.width = 5;
    cv::GaussianBlur(gray, gray_blur, size,1.0, 1.0);
    cv::Canny(gray_blur, canny, 0, 50, 5, true);

    // Find contours
    contours.clear();
    cv::findContours(canny.clone(), contours, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);

    std::vector<cv::Point> approx;

    squares.clear();
    bool test = false;
    for(int i = 0; i < contours.size(); i++) {
        cv::approxPolyDP(cv::Mat(contours[i]), approx, cv::arcLength(cv::Mat(contours[i]), true)*0.02, true);
        if(std::fabs(cv::contourArea(contours[i])) < 100 ||  !cv::isContourConvex(approx)) {
            continue;
        }

/*        // Sample four points, roughly from each side
        if(approx.size() >= 4) {
            std::vector<cv::Point> samples(4);
            samples[0] = approx[approx.size()/8];
            samples[1] = approx[3*approx.size()/8];
            samples[2] = approx[5*approx.size()/8];
            samples[3] = approx[7*approx.size()/8];
        }*/
        if(approx.size() == 4) {

            // Get the cosines of all corners
            std::vector<double> cos;

            // Number of vertices of polygonal curve
            int vtc = approx.size();

            for (int j = 2; j < vtc+1; j++)
                cos.push_back(angle(approx[j%vtc], approx[j-2], approx[j-1]));

            // Sort ascending the cosine values
            std::sort(cos.begin(), cos.end());

            // Get the lowest and the highest cosine
            double mincos = cos.front();
            double maxcos = cos.back();

            // Use the degrees obtained above and the number of vertices
            // to determine the shape of the contour
            if (mincos >= -0.1 && maxcos <= 0.3) {
                squares.push_back(approx);
            }

        }
    }

}

void SquareLoc::showSquares() {

    cv::Mat dst = img.clone();
    cv::Mat cont_img = cv::Mat::zeros( canny.size(), CV_8UC3 );
    const cv::Point* pAll;
    int nAll;

    for( int i = 0; i< contours.size(); i++ ) {
        cv::Scalar color = cv::Scalar( rand()%255, rand()%255, rand()%255 );
//         drawContours( cont_img, contours, i, color, 2, 8, hierarchy, 0, Point() );
         cv::drawContours(cont_img, contours, i,color, 2, 8);
    }

    for( size_t i = 0; i < squares.size(); i++ ){
 //       std::cout << fabs(contourArea(cv::Mat(squares[i]))) << std::endl;

        pAll = &squares[i][0];
        nAll = (int)squares[i].size();
        polylines(dst, &pAll, &nAll, 1, true, cv::Scalar(0,255,0), 2, 3, 0);
    }

    cv::imshow("img", img);
    cv::imshow("contours", cont_img);
    cv::imshow("canny", canny);
    cv::imshow("dst", dst);
    cv::waitKey(0);

}

int main(int argc, char** argv) {

    ros::init(argc, argv, "square_loc");
    cv::Mat img = cv::imread(TEST_IMG_1, CV_LOAD_IMAGE_COLOR);
    SquareLoc sqloc(img);
    sqloc.findSquares();
    sqloc.showSquares();

    img = cv::imread(TEST_IMG_2, CV_LOAD_IMAGE_COLOR);
    sqloc.img = img;
    sqloc.findSquares();
    sqloc.showSquares();

    img = cv::imread(TEST_IMG_3, CV_LOAD_IMAGE_COLOR);
    sqloc.img = img;
    sqloc.findSquares();
    sqloc.showSquares();

    img = cv::imread(TEST_IMG_4, CV_LOAD_IMAGE_COLOR);
    sqloc.img = img;
    sqloc.findSquares();
    sqloc.showSquares();

    img = cv::imread(TEST_IMG_5, CV_LOAD_IMAGE_COLOR);
    sqloc.img = img;
    sqloc.findSquares();
    sqloc.showSquares();

    img = cv::imread(TEST_IMG_6, CV_LOAD_IMAGE_COLOR);
    sqloc.img = img;
    sqloc.findSquares();
    sqloc.showSquares();

    return 0;
}
