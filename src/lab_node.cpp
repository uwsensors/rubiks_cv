#include <rubiks_cv/lab_node.h>

using namespace std;

LABNode::LABNode(unsigned int x, unsigned int y, int* lab): x(x), y(y), lab(lab), cluster(-1) {}
LABNode::~LABNode() {}
void LABNode::assignToCluster(int cluster) {
    this->cluster = cluster;
}
bool LABNode::isSimilarColor(const LABNode& other, double lThresh, double aThresh, double bThresh) {
    return abs(lab[0] - other.lab[0]) < lThresh && abs(lab[1]-other.lab[1]) < aThresh && abs(lab[2]-other.lab[2]) < bThresh;
}
