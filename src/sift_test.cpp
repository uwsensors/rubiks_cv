#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <iostream>
#include <opencv2/nonfree/nonfree.hpp>
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/nonfree/nonfree.hpp"
const static std::string img1_name = "/home/planc509/cube_ws/src/rubiks_cv/data/no_crop/face1.png";
const static std::string img2_name = "/home/planc509/cube_ws/src/rubiks_cv/data/no_crop/cross.png";
const static std::string img3_name = "/home/planc509/cube_ws/src/rubiks_cv/data/cropped2/yellow_square.jpg";
using namespace cv;

void separateColorChannels(Mat& img, std::vector<Mat>& channels) {
    channels.clear();
    for(int i = 0; i < 3; i++) {
        channels.push_back(Mat(img.size(), CV_8U));
    }


}

void getSIFTFeatures(Mat& img, std::vector<KeyPoint>& keypoints, Mat& descriptors) {
    std::vector<Mat> channels;
    separateColorChannels(img,channels);

    SiftDescriptorExtractor ex;
    int n_descriptors = 0;
    for(int i = 0; i < channels.size(); i++) {
        Mat tmp_descriptors;
        ex.compute(channels[i], keypoints, tmp_descriptors);
        if(i == 0) {
            descriptors = tmp_descriptors.clone();
        } else {
            hconcat(tmp_descriptors, descriptors, descriptors);
        }

        n_descriptors += tmp_descriptors.cols;
        if(tmp_descriptors.rows != descriptors.rows) {
            std::cout << "Incorrect number of descriptors" << std::endl;
        }
    }

    if(descriptors.cols != n_descriptors) {
        std::cout << "Incorrect descriptor concatenation" << std::endl;
    }

}

int main(int argc, const char* argv[])
{
    Mat img_scene;
    if(argc >= 2) {
        img_scene = imread(argv[1], CV_LOAD_IMAGE_GRAYSCALE);
    } else {
        img_scene = imread(img1_name, CV_LOAD_IMAGE_GRAYSCALE); //Load as grayscale
    }

    Mat img_obj = imread(img2_name, CV_LOAD_IMAGE_GRAYSCALE); //Load as grayscale

    if(!img_obj.data || !img_scene.data) {
        std::cout << "Could not read images"  << std::endl;
        return -1;
    }

    Mat bw_scene = img_scene > 50;
    Mat bw_obj = img_obj > 50;

    SiftFeatureDetector detector;
    std::vector<KeyPoint> keypoints_scene, keypoints_obj;
    Mat descriptors_scene, descriptors_obj;
    detector.detect(bw_scene, keypoints_scene);
    detector.detect(bw_obj, keypoints_obj);

    cv::SiftDescriptorExtractor ex;
    ex.compute(bw_scene, keypoints_scene, descriptors_scene);
    ex.compute(bw_obj, keypoints_obj, descriptors_obj);

    std::vector<std::vector< DMatch > >knn_matches;
    BFMatcher matcher;
    matcher.knnMatch(descriptors_obj,descriptors_scene, knn_matches, 2 );

    Mat img_matches;

    vector<cv::DMatch> good_matches;
    for (int i = 0; i < knn_matches.size(); ++i)
    {
        const float ratio = 2.0; // As in Lowe's paper; can be tuned
        if (knn_matches[i][0].distance < ratio * knn_matches[i][1].distance)
        {
            good_matches.push_back(knn_matches[i][0]);
        }
    }

    std::cout << "good_matches.size(): " << good_matches.size() << std::endl;
    drawMatches( bw_obj, keypoints_obj, bw_scene, keypoints_scene, good_matches, img_matches );
    imshow("result",img_matches);
    waitKey(0);

    std::vector<Point2f> obj;
    std::vector<Point2f> scene;

    for(int i = 0; i < good_matches.size(); i++) {
        obj.push_back(keypoints_obj[good_matches[i].queryIdx ].pt);
        scene.push_back(keypoints_scene[good_matches[i].trainIdx].pt);
    }

    Mat H = findHomography(obj, scene, CV_RANSAC);

    std::vector<Point2f> obj_corners(4);
    obj_corners[0] = cvPoint(0,0);
    obj_corners[1] = cvPoint(img_obj.cols,0);
    obj_corners[2] = cvPoint(img_obj.cols, img_obj.rows);
    obj_corners[3] = cvPoint(0, img_obj.rows);
    std::vector<Point2f> scene_corners(4);
     perspectiveTransform( obj_corners, scene_corners, H);

     //-- Draw lines between the corners (the mapped object in the scene - image_2 )
      line( img_matches, scene_corners[0] + Point2f( img_obj.cols, 0), scene_corners[1] + Point2f( img_obj.cols, 0), Scalar(0, 255, 0), 4 );
      line( img_matches, scene_corners[1] + Point2f( img_obj.cols, 0), scene_corners[2] + Point2f( img_obj.cols, 0), Scalar( 0, 255, 0), 4 );
      line( img_matches, scene_corners[2] + Point2f( img_obj.cols, 0), scene_corners[3] + Point2f( img_obj.cols, 0), Scalar( 0, 255, 0), 4 );
      line( img_matches, scene_corners[3] + Point2f( img_obj.cols, 0), scene_corners[0] + Point2f( img_obj.cols, 0), Scalar( 0, 255, 0), 4 );

      imshow( "Good Matches & Object detection", img_matches );

      waitKey(0);
      return 0;
    /*
    cv::initModule_nonfree();
    Mat gray_img1 = imread(img1_name, CV_LOAD_IMAGE_GRAYSCALE); //Load as grayscale
    Mat gray_img2 = imread(img2_name, CV_LOAD_IMAGE_GRAYSCALE); //Load as grayscale
    Mat img1 = imread(img1_name, CV_LOAD_IMAGE_COLOR);
    Mat img2 = imread(img2_name, CV_LOAD_IMAGE_COLOR);
    namedWindow("Display window", cv::WINDOW_NORMAL);

    SiftFeatureDetector detector;
    std::vector<KeyPoint> keypoints1, keypoints2;
    Mat descriptors1, descriptors2;
    detector.detect(gray_img1, keypoints1);
    detector.detect(gray_img2, keypoints2);

    cv::Ptr<cv::DescriptorExtractor> oppDescExtractor= new cv::SiftDescriptorExtractor;
    cv::OpponentColorDescriptorExtractor opponentDescExtractor(oppDescExtractor);
    opponentDescExtractor.compute(img1, keypoints1, descriptors1);
    opponentDescExtractor.compute(img2, keypoints2, descriptors2);
    std::cout << descriptors1.size() << std::endl;
    std::cout << descriptors2.size() << std::endl;
    BFMatcher matcher;
  //  cv::Ptr<cv::DescriptorMatcher> matcher = DescriptorMatcher::create("ORB");

    std::vector< DMatch > matches;

    matcher.match( descriptors1, descriptors2, matches );

    float best_dist = matches[0].distance;
    int best_idx = 0;
    for(int i = 0; i < matches.size(); i++) {
        if(matches[i].distance < best_dist) {
            best_dist = matches[i].distance;
            best_idx = i;
        }
    }
    std::vector<DMatch> best_match;
    best_match.push_back(matches[best_idx]);

    std::vector<std::vector< DMatch > >knn_matches;

    matcher.knnMatch(descriptors2,descriptors1, knn_matches, 2 );
    //-- Draw matches
 //   keypoints1.clear();
 //   keypoints2.clear();
    Mat img_matches;

    vector<cv::DMatch> good_matches;
    for (int i = 0; i < knn_matches.size(); ++i)
    {
        const float ratio = 2.0; // As in Lowe's paper; can be tuned
        if (knn_matches[i][0].distance < ratio * knn_matches[i][1].distance)
        {
            good_matches.push_back(knn_matches[i][0]);
        }
    }
    std::cout << "good_matches.size(): " << good_matches.size() << std::endl;
    drawMatches( img2, keypoints2, img1, keypoints1, knn_matches, img_matches );

    //-- Show detected matches
    imshow("Matches", img_matches );

    waitKey(0);*/

/*
    SiftDescriptorExtractor ex;
    ex.compute(input, keypoints,);
    // Add results to image and save.
    Mat output;
    drawKeypoints(input, keypoints, output);
    namedWindow("Display window", cv::WINDOW_NORMAL);

    imshow("Display window", input);
    waitKey(0);

    imshow("Display window", output);
    waitKey(0);*/
//    cv::imwrite("sift_result.jpg", output);

    return 0;
}
