#include <rubiks_cv/euclideanCluster.h>

const static int COLOR_THRESHOLD = 4;

void euclideanCluster(Mat &img, vector<Cluster*>& clusters) {
    NNGrid* grid = new NNGrid(img);

    queue<Node*> insideCluster;
    queue<Node*> outsideCluster;
    vector<double*> avgClusterColors;
    vector<int> clusterSizes;
    vector<double> avgClusterX;
    vector<double> avgClusterY;
    int nClusters = 0;

    unsigned int x = (unsigned int) (rand() % img.cols);
    unsigned int y = (unsigned int) (rand() % img.rows);

    outsideCluster.push(grid->getNode(x,y));

    while(!outsideCluster.empty()) {
        Node* temp = NULL;
        while(!outsideCluster.empty()) {
            temp = outsideCluster.front();
            outsideCluster.pop();
            if(temp->cluster < 0) {
                break;
            } else {
                temp = NULL;
            }
        }
        if(temp == NULL) {
            break;
        }
        temp->assignToCluster(nClusters);
        insideCluster.push(temp);
        double* rgb = new double[img.channels()];
        for(int i = 0; i < img.channels(); i++) {
            rgb[i] = 0.0;
        }
        avgClusterColors.push_back(rgb);
        clusterSizes.push_back(0);
        avgClusterX.push_back(0.0);
        avgClusterY.push_back(0.0);

        while(!insideCluster.empty()) {

            Node* pixel = insideCluster.front();

            insideCluster.pop();
            vector<Node*> neighbors;
            grid->getNeighbors(pixel->x, pixel->y, neighbors);

            for(int i = 0; i < img.channels(); i++) {
                rgb[i] += pixel->vals[i];
            }
            clusterSizes[nClusters]++;
            avgClusterX[nClusters]+=pixel->x;
            avgClusterY[nClusters]+=pixel->y;
            for(int i = 0; i < neighbors.size(); i++) {
                if(pixel->isSimilarColor(*neighbors[i], COLOR_THRESHOLD, COLOR_THRESHOLD, COLOR_THRESHOLD)){
                    neighbors[i]->assignToCluster(nClusters);
                    insideCluster.push(neighbors[i]);
                } else {
                    outsideCluster.push(neighbors[i]);
                }
            }
        }
        nClusters++;
    }

    for(int i = 0; i < nClusters; i++) {
        double* rgb = avgClusterColors[i];
        int* color = new int[img.channels()];

        for(int j = 0; j < img.channels(); j++) {

            color[j] = (int) (rgb[j] / clusterSizes[i]);
           // color[j] = (int) (rand()%256);
        }
        delete [] rgb;
        avgClusterX[i] = avgClusterX[i]/clusterSizes[i];
        avgClusterY[i] = avgClusterY[i]/clusterSizes[i];

        clusters.push_back(new Cluster(color, clusterSizes[i], avgClusterX[i], avgClusterY[i]));
    }

    if(EUCLIDEAN_DEBUG) {
        for(int i = 0; i < img.rows; i++) {
            for(int j = 0; j < img.cols; j++) {
                int clusterId = grid->getNode(j/img.channels(),i)->cluster;
                Cluster* cluster = clusters[clusterId];
                Vec3b vals(cluster->avgColor[0], cluster->avgColor[1], cluster->avgColor[2]);
                img.at<Vec3b>(i,j) = vals;
            }
        }
    }
    delete grid;
}


