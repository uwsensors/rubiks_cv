#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <ros/ros.h>
#include <tf/transform_listener.h>

using namespace cv;
using namespace std;

int main(int argc, char** argv) {

	ros::init(argc, argv, "train_ridge");
	ros::NodeHandle node;
    if(argc != 2) {
        cout << "Need filename" << endl;
         return -1;
    }
    Mat image;
    image = imread(argv[1], CV_LOAD_IMAGE_COLOR);
    if(!image.data) {
        cout << "Could not open or find the image" << endl;
        return -1;
    }
    uchar* pixel = image.data;
    int channels = image.channels();
    int nRows = image.rows;
    int nCols = image.cols;
    ROS_INFO("channels = %d", channels);
    ROS_INFO("rows = %d", nRows);
    ROS_INFO("nCols = %d", nCols);
    for(unsigned int i = 0; i < nCols*nRows; i++) {
        uchar tmp = *(pixel+2);
        *(pixel+2) = *(pixel+1);
        *(pixel+1) = *pixel;
        *pixel = tmp;

        pixel+=3;
    }
    namedWindow("Display window", WINDOW_NORMAL);
    imshow("Display window", image);
    waitKey(0);
    while(ros::ok());

	return 0;

}
