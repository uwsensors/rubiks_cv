#include <rubiks_cv/cluster.h>

Cluster::Cluster(const int* avgColor, int assignedColor): avgColor(NULL),size(0),x(0),y(0),assignedColor(assignedColor),photoID(-1),meanDist(-1.0){
    this->avgColor = (int*) malloc(3*sizeof(char));
    for(int i = 0; i < 3; i++) {
        this->avgColor[i] = avgColor[i];
    }
}
Cluster::Cluster(const int *avgColor, unsigned int size, double x, double y):avgColor(NULL),size(size),x(x),y(y),assignedColor(-1),photoID(-1),meanDist(-1.0){
    this->avgColor = (int*) malloc(3*sizeof(char));
    for(int i = 0; i < 3; i++) {
        this->avgColor[i] = avgColor[i];
    }
}
Cluster::~Cluster(){

    if(this->avgColor) {
        free(this->avgColor);
    }
}
