#include <rubiks_cv/lab_glare_correction.h>

const static int ILLUM_LOWER_BOUND = 75;

void LABGlareCorrection::removeGlare(Mat& img_in, Mat& img_out) {

    LABGrid grid(img_in);

    int min_l = INT_MAX;
    int max_l = INT_MIN;
    int min_a = INT_MAX;
    int max_a = INT_MIN;
    int max_b = INT_MIN;
    int min_b = INT_MAX;

    for(int i = 0; i < img_in.rows; i++) {
        for(int j = 0; j < img_in.cols; j++) {
            LABNode* node = grid.getNode(j,i);
            if(node->lab[0] < min_l) {
                min_l = node->lab[0];
            } else if(node->lab[0] > max_l) {
                max_l = node->lab[0];
            }

            if(node->lab[1] < min_a) {
                min_a = node->lab[1];
            } else if(node->lab[1] > max_a) {
                max_a = node->lab[1];
            }

            if(node->lab[2] < min_b) {
                min_b = node->lab[2];
            } else if(node->lab[2] > max_b) {
                max_b = node->lab[2];
            }
        }
    }

    std::cout << "min_l = " << min_l << "; max_l = " << max_l << std::endl;
    std::cout << "min_a = " << min_a << "; max_a = " << max_a << std::endl;
    std::cout << "min_b = " << min_b << "; max_b = " << max_b << std::endl;

    for(int i = 0; i < img_in.rows; i++) {
        for(int j = 0; j < img_in.cols; j++) {
            fixCluster(grid, j, i);
        }
    }

    grid.gridToImg(img_out);
}

void LABGlareCorrection::fixCluster(LABGrid& grid, unsigned int x, unsigned int y) {

    LABNode* cur_node = grid.getNode(x,y);
    if(!LABGlareCorrection::isGlare(cur_node)) {
        return;
    }
    cur_node->assignToCluster(0);
    std::queue<LABNode*> insideCluster;
    std::queue<LABNode*> storage;
    insideCluster.push(cur_node);
    int max_l = -1000;
    int max_a = -1000;
    int max_b = -1000;
    float max_mag = 0;
    int avg_l = 0;
    int avg_a = 0;
    int avg_b = 0;
    float count = 0;
    while(!insideCluster.empty()) {
        LABNode* cur_node = insideCluster.front();
        insideCluster.pop();
        storage.push(cur_node);
        std::vector<LABNode*> neighbors;
        grid.getNeighbors(cur_node->x,cur_node->y, neighbors);

        for(int i = 0; i < neighbors.size(); i++) {
            if(LABGlareCorrection::isGlare(neighbors[i])) {
                neighbors[i]->assignToCluster(0);
                insideCluster.push(neighbors[i]);
            } else {
   //             neighbors[i]->assignToCluster(1);
                int new_neighbor_x = neighbors[i]->x + rand() % 5;
                int new_neighbor_y = neighbors[i]->y + rand() % 5;
                LABNode* new_neighbor = grid.getNode(new_neighbor_x, new_neighbor_y);
                avg_l += neighbors[i]->lab[0];
                avg_a += neighbors[i]->lab[1];
                avg_b += neighbors[i]->lab[2];
                count++;
                float mag = std::pow(new_neighbor->lab[1]-127,2) + std::pow(new_neighbor->lab[2]-127,2);
                if(mag > max_mag) {

                    max_mag = mag;
                    max_l = 255;//new_neighbor->lab[0];
                    max_a = 0;//new_neighbor->lab[1];
                    max_b = 0;//new_neighbor->lab[2];

//                    max_l = rand()%256;
//                    max_a = rand()%256;
//                    max_b = rand()%256;
                }
            }
        }
    }

    while(!storage.empty()) {
        cur_node = storage.front();
        storage.pop();
        if(max_l < 10) {
   //         std::cout << "Max_l is zero" << std::endl;
        }
//        cur_node->lab[0] = avg_l/count;//0;//max_l;
//        cur_node->lab[1] = avg_a/count;
//        cur_node->lab[2] = avg_b/count;//max_b;

        cur_node->lab[0] = 254;//;//0;//max_l;
        cur_node->lab[1] = 128;//max_a;
        cur_node->lab[2] = 128;//max_b;

    }
}

bool LABGlareCorrection::isGlare(LABNode *node) {
    return node->lab[0] <= ILLUM_LOWER_BOUND;
}

const static double SIGMA = 1.0;
const static std::string img1_name = "/home/planc509/cube_ws/src/rubiks_cv/data/cropped2/face5.png";
int main(int argc, char** argv) {

/*    Mat img1 = imread(img1_name, CV_LOAD_IMAGE_COLOR);
    Mat lab_img = img1.clone();
    cvtColor(img1, lab_img, CV_BGR2Lab);
    LABGrid grid(lab_img);
    Mat tmp;
    grid.gridToImg(tmp);
    Mat fixed = img1.clone();
    cvtColor(tmp, fixed, CV_Lab2BGR);

    imshow("original", img1);
    imshow("fixed", fixed);

    waitKey(0);*/

    Mat img1 = imread(img1_name, CV_LOAD_IMAGE_COLOR);

    Mat lab_img = img1.clone();

    cvtColor(img1, lab_img, CV_BGR2Lab);

    Mat lab_img_no_glare;
    LABGlareCorrection::removeGlare(lab_img, lab_img_no_glare);

    Mat fixed = img1.clone();
    cvtColor(lab_img_no_glare, fixed, CV_Lab2BGR);
    Mat blur1;
    GaussianBlur( fixed, blur1, Size( 0, 0 ), SIGMA, SIGMA );
    imshow("original", img1);
    imshow("original_lab", lab_img);
    imshow("fixed_lab", lab_img_no_glare);
    imshow("fixed", blur1);
    waitKey(0);
}
