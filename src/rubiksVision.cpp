#include <ros/ros.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <rubiks_cv/square_cluster.h>
#include <rubiks_cv/CubeState.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <algorithm>

using namespace cv;
using namespace std;

const static string TEST_IMG_1 = "/home/planc509/cube_ws/src/rubiks_cv/data/cropped7/face1.png";
const static string TEST_IMG_2 = "/home/planc509/cube_ws/src/rubiks_cv/data/cropped7/face2.png";
const static string TEST_IMG_3 = "/home/planc509/cube_ws/src/rubiks_cv/data/cropped7/face3.png";
const static string TEST_IMG_4 = "/home/planc509/cube_ws/src/rubiks_cv/data/cropped7/face4.png";
const static string TEST_IMG_5 = "/home/planc509/cube_ws/src/rubiks_cv/data/cropped7/face5.png";
const static string TEST_IMG_6 = "/home/planc509/cube_ws/src/rubiks_cv/data/cropped7/face6.png";

bool offer_service;
bool save;
bool check_vision_result;

std::string save_prefix;

bool checkFace(char response, std::string& result) {
	
	response = response % 256;
	std::string tmp_result("");

	while(response != 'y' && response != 'Y' && response != 'n' && response != 'N') {
		std::cout << "Enter y if correct, n if incorrect" << std::endl<<std::endl;
		response = waitKey(0) % 256;
	}

	if(response != 'y' && response != 'Y' ) { 
		std::cout << "Please enter the correct cube-state, top to bottom, left to right" << std::endl;
		std::cout << "R/r = Red, O/o = Orange" << std::endl;
		std::cout << "Y/y = Yellow, G/g = Green" << std::endl;
		std::cout << "B/b = Blue, W/w = White" << std::endl;

		while(1) {
			std::getline(cin, tmp_result);
			transform(tmp_result.begin(), tmp_result.end(), tmp_result.begin(), ::toupper);
			int i = 0;
			for(; i < tmp_result.size(); i++) {
        if(tmp_result[i] != 'R' && tmp_result[i] != 'O' && tmp_result[i] != 'Y'
						&& tmp_result[i] != 'G' && tmp_result[i] != 'B' && tmp_result[i] != 'W') {
					std::cout << "Invalid characters" << std::endl<<std::endl;
					i = -1;			
					break;
				} 
			} 
			if(i != 9) {
				std::cout << "Please enter 9 letters [R,O,Y,G,B,W]" << std::endl<<std::endl;
				continue;
			} 
			break;
		}
		std::cout << std::endl;
		result = tmp_result;
	}

	return true;
}

std::string computeCubeState(Mat& img1, Mat& img2, Mat& img3, Mat& img4, Mat& img5, Mat& img6) {

    Mat luv_img1 = img1.clone();
    cvtColor(img1, luv_img1, CV_BGR2Luv);

    Mat luv_img2 = img2.clone();
    cvtColor(img2, luv_img2, CV_BGR2Luv);

    Mat luv_img3 = img3.clone();
    cvtColor(img3, luv_img3, CV_BGR2Luv);

    Mat luv_img4 = img4.clone();
    cvtColor(img4, luv_img4, CV_BGR2Luv);

    Mat luv_img5 = img5.clone();
    cvtColor(img5, luv_img5, CV_BGR2Luv);

    Mat luv_img6 = img6.clone();
    cvtColor(img6, luv_img6, CV_BGR2Luv);

    std::string result1 = SquareCluster::squareCluster(luv_img1);
//    std::cout << "Finished image 1: " << result1 << std::endl;

    std::string result2 = SquareCluster::squareCluster(luv_img2);
//    std::cout << "Finished image 2: " << result2 << std::endl;

    std::string result3 = SquareCluster::squareCluster(luv_img3);
//    std::cout << "Finished image 3: " << result3 << std::endl;

    std::string result4 = SquareCluster::squareCluster(luv_img4);
//    std::cout << "Finished image 4: " << result4 << std::endl;

    std::string result5 = SquareCluster::squareCluster(luv_img5);
//    std::cout << "Finished image 5: " << result5 << std::endl;

    std::string result6 = SquareCluster::squareCluster(luv_img6);
//    std::cout << "Finished image 6: " << result6 << std::endl;

    if(check_vision_result) {
				char response = ' ';
        imshow("Display window", img1);
        for(int i = 0; i < 9; i+=3) {
            std::cout << result1.substr(i, std::min(3, ((int)result1.size())-i)) << std::endl;
        }
        std::cout << std::endl;
        response = waitKey(0);
				checkFace(response, result1);

        imshow("Display window", img2);
        for(int i = 0; i < 9; i+=3) {
            std::cout << result2.substr(i, std::min(3, ((int)result2.size())-i)) << std::endl;
        }
        std::cout << std::endl;
        response = waitKey(0);
				checkFace(response, result2);

        imshow("Display window", img3);
        for(int i = 0; i < 9; i+=3) {
            std::cout << result3.substr(i, std::min(3, ((int)result3.size())-i)) << std::endl;
        }
        std::cout << std::endl;
        response = waitKey(0);
				checkFace(response, result3);

        imshow("Display window", img4);
        for(int i = 0; i < 9; i+=3) {
            std::cout << result4.substr(i, std::min(3, ((int)result4.size())-i)) << std::endl;
        }
        std::cout << std::endl;
        response = waitKey(0);
				checkFace(response, result4);

        imshow("Display window", img5);
        for(int i = 0; i < 9; i+=3) {
            std::cout << result5.substr(i, std::min(3, ((int)result5.size())-i)) << std::endl;
        }
        std::cout << std::endl;
        response = waitKey(0);
				checkFace(response, result5);

        imshow("Display window", img6);
        for(int i = 0; i < 9; i+=3) {
            std::cout << result6.substr(i, std::min(3, ((int)result6.size())-i)) << std::endl;
        }
        std::cout << std::endl;
        response = waitKey(0);
				checkFace(response, result6);

    }
		std::reverse(result6.begin(), result6.end());  // Need to rotate last picture result
		std::string result = "L:" + result1 +
                         " R:"+ result2 + 
                         " U:"+ result3 + 
                         " D:"+ result4 + 
                         " F:"+ result5 + 
                         " B:"+ result6;
		std::cout << "Going to return: " << result << std::endl;
		return result;
    
}

bool cubeStateService(rubiks_cv::CubeState::Request &req, rubiks_cv::CubeState::Response& res) {

    const sensor_msgs::Image constImg1 = req.img1;
    const sensor_msgs::Image constImg2 = req.img2;
    const sensor_msgs::Image constImg3 = req.img3;
    const sensor_msgs::Image constImg4 = req.img4;
    const sensor_msgs::Image constImg5 = req.img5;
    const sensor_msgs::Image constImg6 = req.img6;

    cv_bridge::CvImagePtr cv1 = cv_bridge::toCvCopy(constImg1, sensor_msgs::image_encodings::BGR8);
    cv_bridge::CvImagePtr cv2 = cv_bridge::toCvCopy(constImg2, sensor_msgs::image_encodings::BGR8);
    cv_bridge::CvImagePtr cv3 = cv_bridge::toCvCopy(constImg3, sensor_msgs::image_encodings::BGR8);
    cv_bridge::CvImagePtr cv4 = cv_bridge::toCvCopy(constImg4, sensor_msgs::image_encodings::BGR8);
    cv_bridge::CvImagePtr cv5 = cv_bridge::toCvCopy(constImg5, sensor_msgs::image_encodings::BGR8);
    cv_bridge::CvImagePtr cv6 = cv_bridge::toCvCopy(constImg6, sensor_msgs::image_encodings::BGR8);
    if(save) {
        try {
            ROS_INFO("Going to save images in %s", save_prefix.c_str());
            imwrite( save_prefix + "/img1.png", cv1->image );
            imwrite( save_prefix + "/img2.png", cv2->image );
            imwrite( save_prefix + "/img3.png", cv3->image );
            imwrite( save_prefix + "/img4.png", cv4->image );  
            imwrite( save_prefix + "/img5.png", cv5->image );
            imwrite( save_prefix + "/img6.png", cv6->image );

        }  catch (cv_bridge::Exception& e) {
            ROS_ERROR("cv_bridge exception: %s", e.what());
            return false;
        }
    }
    res.state = computeCubeState(cv1->image,cv2->image,cv3->image,cv4->image,cv5->image, cv6->image);
    return true;
}

int main(int argc, char** argv) {

    ros::init(argc, argv, "rubiks_vision");
    ros::NodeHandle node;
    offer_service = true;
    save = false;
		check_vision_result = true;
    save_prefix = "";
    node.param("/rubiks_vision/offer_service", offer_service, offer_service);
    node.param("/rubiks_vision/save_images", save, save);
		node.param("/rubiks_vision/check_vision_result", check_vision_result, check_vision_result);
    node.param("/rubiks_vision/save_prefix", save_prefix, save_prefix);

    ROS_INFO("offer_service: %d", offer_service);
    ROS_INFO("save_images: %d", save);
	  ROS_INFO("check_vision_result: %d", check_vision_result); 
    ROS_INFO("save_prefix: %s", save_prefix.c_str());

    ros::ServiceServer cubeService;
    if(!offer_service) {
        ROS_INFO("Loading Images...");
        Mat img1 = imread(TEST_IMG_1, CV_LOAD_IMAGE_COLOR);
        Mat img2 = imread(TEST_IMG_2, CV_LOAD_IMAGE_COLOR);
        Mat img3 = imread(TEST_IMG_3, CV_LOAD_IMAGE_COLOR);
        Mat img4 = imread(TEST_IMG_4, CV_LOAD_IMAGE_COLOR);
        Mat img5 = imread(TEST_IMG_5, CV_LOAD_IMAGE_COLOR);
        Mat img6 = imread(TEST_IMG_6, CV_LOAD_IMAGE_COLOR);
        ROS_INFO("...done loading images");

        computeCubeState(img1, img2, img3, img4, img5, img6);

    } else {
        cubeService = node.advertiseService("get_cube_state", cubeStateService);
        ROS_INFO("get_cube_state service is active");
    }

    ros::spin();

}
