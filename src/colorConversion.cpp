#include "rubiks_cv/colorConversion.h"

void ColorConversion::bgrToLABImg(Mat& bgr_img, Mat& lab_img) {
    int* bgr_pixel;
    int* lab_pixel;

    int* bgr = new int[bgr_img.channels()];
    int* lab = new int[lab_img.channels()];
    for(int i = 0; i < bgr_img.rows; i++) {
        bgr_pixel = bgr_img.ptr<int>(i);
        lab_pixel = lab_img.ptr<int>(i);
        for(int j = 0; j < bgr_img.cols*bgr_img.channels(); j+=bgr_img.channels()) {

            for(int k = 0; k < bgr_img.channels(); k++) {
                bgr[k] = bgr_pixel[j+k];
            }
            ColorConversion::bgrToLABPixel(bgr, lab);
            for(int k = 0; k < lab_img.channels(); k++) {
                lab_pixel[j+k] = lab[k];
            }
        }

    }

    if(bgr_to_LAB_debug) {
        imshow("Display window", lab_img);
        waitKey(0);
    }
}

void ColorConversion::bgrToLABPixel(const int *bgr, int *lab) {

    float xyz[3];
    float rgb_float[3];

    for(int i = 0; i < 3; i++) {
        rgb_float[i] = bgr[3-i]/256.0;
    }
    for(int i = 0; i < 3; i++) {
        xyz[i] = 0.0;
        for(int j = 0; j < 3; j++) {
            xyz[i]+= bgr_to_XYZ_params[i][j]*rgb_float[j];
        }
    }
    xyz[0] = xyz[0]/x_normal;
    xyz[2] = xyz[2]/z_normal;

    if(xyz[1] > l_limit) {
        lab[0] = (int) (116*std::pow(xyz[1],1.0/3)-16);
    } else {
        lab[0] = (int) (903.3*xyz[1]);
    }

    for(int i = 0; i < 3; i++) {
        if(xyz[i] > f_limit) {
            xyz[i] = std::pow(xyz[i],1.0/3);
        } else {
            xyz[i] = 7.787*xyz[i]+(16.0/116);
        }
    }

    lab[1] = alpha_coeff*(xyz[0]-xyz[1])+128;
    lab[2] = beta_coeff*(xyz[1]-xyz[2])+128;

    for(int i = 0; i < 3; i++) {
        if(lab[i] < 0 || lab[i] >= 256) {
            std::cout << "LAB  val out of range: L = "
                      << lab[0] << ", a = " << lab[1]
                      << ", b = " << lab[2] << std::endl;
            break;
        }
    }

}

void ColorConversion::getLabRefColors(std::vector<std::vector<int> >& ref_colors) {
    ref_colors.clear();
    ref_colors.resize(6);
    for(int i = 0; i < 6; i++) {
        ref_colors[i].resize(3);
        for(int j = 0; j < 3; j++) {
            ref_colors[i][j] = lab_ref_colors[i][j];
        }
    }
}
