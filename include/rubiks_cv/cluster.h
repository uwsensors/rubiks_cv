#ifndef CLUSTER_H
#define CLUSTER_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

class Cluster {
    public: int* avgColor;
            unsigned int size;
            double x;
            double y;
            int assignedColor;
            int photoID;
            double meanDist;

            Cluster(const int* avgColor, int assignedColor);
            Cluster(const int* avgColor, unsigned int size, double x, double y);
            ~Cluster();
};

#endif // CLUSTER_H
