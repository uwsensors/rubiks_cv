#ifndef SQUARE_CLUSTER_H
#define SQUARE_CLUSTER_H

#include <rubiks_cv/nnGrid.h>
#include <rubiks_cv/cluster.h>
#include <climits>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

class SquareCluster{

public:
    /** Get string representing colors in passed image **/
    static std::string squareCluster(Mat& img);

private:
    static int green_ref[];
    static int red_ref[];
    static int blue_ref[];
    static int yellow_ref[];
    static int orange_ref[];
    static int white_ref[];
    static int* ref_colors[];

    const static int CROP_LIGHT_THRESHOLD;
    const static int DARK_EXPANSION_THRESHOLD;

    /** Build the result string from the found color indices **/
    static std::string parseResult(int* colors);

    /** Print result to the console **/
    static void printResult(int* colors);

    /** Find the crop edge by inching towards the crop-image intersection
      grid: The grid representing the image
      x: The x coordinate to start the search at
         Will hold x coordinate of crop edge upon return
      y: The y coordinate to start the search at
         Will hold y coordinate of crop edge upon return
      dx: How much to increment in x direction for each iteration
      dy: How much to increment in y direction for each iteration **/
    static bool findCropEdge(NNGrid& grid, int& x,int& y, int dx, int dy);

    /** Find the edge of a square by inching towards a dark edge
      grid: The grid representing the image
      x: The x coordinate to start the search at
         Will hold x coordinate of edge upon return
      y: The y coordinate to start the search at
         Will hold y coordinate of edge upon return
      dx: How much to increment in x direction for each iteration
      dy: How much to increment in y direction for each iteration
      itr_limit: Limits the number of iterations done in any direction **/
    static int* findColorEdge(NNGrid& grid, int& x,int& y, int dx, int dy, int itr_limit = -1);

    /** Returns the index that the passed color is closest too **/
    static int getClosestRef(int* color);

    /** Iterate horizontally and vertically to find the dominant color in the square
        grid: Represents the image
        x_query: Approximate x coordinate of the center of the square
        y_query: Approximate y coordinate of the center of the square
        itr_limit: Limits the number of iterations allowed **/
    static int getDominantColor(NNGrid& grid, int x_query, int y_query, int itr_limit);

    /** Expand horizontally and vertically until a dark pixel is encountered
        grid: Represents the image to be searched
        x_query: Approximate x coordinate of center
        y_query: Approximate y coordinate of center **/
    static Cluster expandUntilDark(NNGrid& grid, int x_query, int y_query);

};

#endif // SQUARE_CLUSTER_H
