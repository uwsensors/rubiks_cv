#ifndef COLORCONVERSION_H
#define COLORCONVERSION_H
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>

using namespace cv;

const static bool bgr_to_LAB_debug = false;
const static float bgr_to_XYZ_params[3][3] = {
    {0.412453,0.35758,0.180423},
    {0.212671,0.715160,0.072169},
    {0.019334,0.119193,0.950227}
};
const static int lab_ref_colors[6][3] = {
    {136,208,195},
    {142,201,196},
    {94,84,170},
    {215,132,213},
    {66,157,69},
    {255,128,128}
};
const static float x_normal = 0.950456;
const static float z_normal = 1.088754;
const static float l_limit = 0.008856;
const static float f_limit = 0.008856;
const static float alpha_coeff = 500;
const static float beta_coeff = 200;

class ColorConversion{
public:

    static void bgrToLABImg(Mat& bgr_img, Mat& lab_img);
    // Source: http://docs.opencv.org/2.4/modules/imgproc/doc/miscellaneous_transformations.html
    static void bgrToLABPixel(const int* bgr, int* lab);
    static void getLabRefColors(std::vector<std::vector<int> >& ref_colors);

};

#endif // COLORCONVERSION_H
