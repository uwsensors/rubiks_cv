#ifndef EUCLIDEANCLUSTER_H
#define EUCLIDEANCLUSTER_H

#define EUCLIDEAN_DEBUG true

#include <queue>
#include <rubiks_cv/node.h>
#include <rubiks_cv/cluster.h>
#include <rubiks_cv/nnGrid.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace std;
using namespace cv;

void euclideanCluster(Mat& img, vector<Cluster*>& clusters);

#endif // EUCLIDEANCLUSTER_H
