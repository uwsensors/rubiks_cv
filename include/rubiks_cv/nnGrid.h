#ifndef NNGRID_H
#define NNGRID_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <rubiks_cv/node.h>

using namespace cv;
using namespace std;

class NNGrid{

    private: Node** grid;
            unsigned int w;
            unsigned int h;
            unsigned int channels;
            int type;

    public: NNGrid(Mat& img);
            ~NNGrid();
            void getNeighbors(unsigned int x, unsigned int y, vector<Node*>& neighbors);
            Node* getNode(unsigned int x, unsigned int y);
            void gridToImg(Mat& img_out);
            int width();
            int height();
};




#endif // NNGRID_H
