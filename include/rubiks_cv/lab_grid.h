#ifndef LAB_GRID_H
#define LAB_GRID_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <rubiks_cv/lab_node.h>

using namespace cv;
using namespace std;

class LABGrid{

    private: LABNode** grid;
            unsigned int w;
            unsigned int h;
            unsigned int channels;
            int type;

    public: LABGrid(Mat& img);
            ~LABGrid();
            void getNeighbors(unsigned int x, unsigned int y, vector<LABNode*>& neighbors);
            LABNode* getNode(unsigned int x, unsigned int y);
            void gridToImg(Mat& img_out);

};

#endif // LAB_GRID_H
