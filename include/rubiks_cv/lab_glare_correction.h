#ifndef LAB_GLARE_CORRECTION_H
#define LAB_GLARE_CORRECTION_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <rubiks_cv/lab_grid.h>
#include <queue>
#include <iostream>
#include <climits>
using namespace cv;

class LABGlareCorrection{
public:
    static void removeGlare(Mat& img_in, Mat &img_out);

private:
//    static const int green_ref[3] = {128, 0, 128};
//    static const int blue_ref[3] = {128, 128, 0};
//    static const int red_red[3] = {128,255,128};
//    static const int yellow_ref[3] = {128, 128, 255};

    static void fixCluster(LABGrid& grid, unsigned int x, unsigned int y);
    static bool isGlare(LABNode* node);
};

#endif // LAB_GLARE_CORRECTION_H
