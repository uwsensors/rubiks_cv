#ifndef NODE_H
#define NODE_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
class Node {
public:
    unsigned int x;
    unsigned int y;
    int* vals;
    int cluster;

    Node(unsigned int x, unsigned int y, int* vals);
    ~Node();
    void assignToCluster(int cluster);
    bool isSimilarColor(const Node& other, double rThresh, double gThresh, double bThresh);
};

#endif
