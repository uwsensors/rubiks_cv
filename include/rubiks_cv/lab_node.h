#ifndef LAB_NODE_H
#define LAB_NODE_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
class LABNode {
public:
    unsigned int x;
    unsigned int y;
    int* lab;
    int cluster;

    LABNode(unsigned int x, unsigned int y, int* lab);
    ~LABNode();
    void assignToCluster(int cluster);
    bool isSimilarColor(const LABNode& other, double lThresh, double aThresh, double bThresh);
};


#endif // LAB_NODE_H
