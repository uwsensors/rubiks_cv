#ifndef KMEANS_H
#define KMEANS_H

#define KMEANS_DEBUG true

#include <ros/ros.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <rubiks_cv/cluster.h>

using namespace std;

class KMeans{
public:
    static void kMeans(vector<Cluster*>& clusters,
                       vector<vector<Cluster*>*>& cube, int k,
                       std::vector<std::vector<int> > ref_colors = std::vector<std::vector<int> >());

private:
    static void initMeans(std::vector<Cluster*>& means, std::vector<std::vector<int> > ref_colors);
    static int assignClusters(vector<Cluster*>& clusters, vector<Cluster*>& means);
    static void updateMeans(vector<Cluster*>& clusters, vector<Cluster*>& means);
    static double distance(Cluster* x, Cluster* mean);
    static void getCandidates(vector<Cluster*>& clusters, vector<vector<Cluster*>*>& candidates);
    static void organizeCandidatesByPhoto(vector<vector<Cluster*>*>& candidates,vector<vector<Cluster*>*>& cube);
};

#endif // KMEANS_H
